import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Card from "./Card";

function List(props) {
  const { search } = props;

  const [list, setList] = useState([]);

  useEffect(() => {
    const myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Zoho-authtoken " + process.env.REACT_APP_AUTH_TOKEN
    );

    fetch(
      new Request("/api", {
        method: "GET",
        headers: myHeaders,
      })
    )
      .then((res) => res.json())
      .then((res) => {
        setList(res.organizations);
      });
  }, []);

  return (
    <div className="mb-3 mx-3">
      {list
        .filter(
          (item) =>
            item.contact_name.toLowerCase().indexOf(search.toLowerCase()) > -1
        )
        .map((item) => (
          <Card
            key={item.organization_id}
            name={item.contact_name}
            email={item.email}
            type={item.sales_tax_type}
            date={item.account_created_date_formatted}
          />
        ))}
    </div>
  );
}

List.propTypes = {
  list: PropTypes.array,
};

export default List;
