import React, { useState } from "react";
import SearchBar from "./SearchBar";
import List from "./List";

export default function Content() {
  const [search, setSearch] = useState("");
  return (
    <div className="container px-3 max-w-screen-md mx-auto md:px-0">
      <SearchBar search={search} searchHandler={setSearch} />
      <List search={search} />
    </div>
  );
}
