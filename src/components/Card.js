import React from "react";

function Card(props) {
  const { name, email, type, date } = props;
  return (
    <div className="bg-gray-100 text-gray-800 p-4 rounded-md my-6 flex flex-col md:flex-row md:items-center shadow-md">
      <figure className="mx-auto mb-3 md:mb-0 rounded-full overflow-hidden w-20 h-20 md:ml-0 md:mr-20 md:w-32 md:h-32">
        <img
          src={`https://ui-avatars.com/api/?background=random&name=${name}&size=200`}
          alt=""
          className="max-w-full max-h-full"
        />
      </figure>
      <div className="md:flex-grow">
        <h2 className="text-lg text-gray-900 text-center md:text-left mb-3 md:text-2xl">
          {name}
        </h2>
        <p className="text-gray-600 italic text-sm text-center md:text-left md:text-lg">
          Email: {email}
        </p>
        <p className="text-gray-600 italic text-sm text-center md:text-left md:text-lg">
          Type: {type}
        </p>
        <p className="text-gray-600 italic text-sm text-center md:text-left md:text-lg">
          Created on: {date ? date : "--"}
        </p>
      </div>
    </div>
  );
}

export default Card;
