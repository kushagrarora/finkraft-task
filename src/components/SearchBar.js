import React, { useRef } from "react";

export default function SearchBar(props) {
  const { searchHandler } = props;

  const inputRef = useRef(null);

  const submitHandler = (ev) => {
    ev.preventDefault();

    searchHandler(inputRef.current.value);
  };

  return (
    <form
      className="flex mb-5 text-sm mt-2 md:text-base"
      onSubmit={submitHandler}
    >
      <input
        type="text"
        ref={inputRef}
        className="p-2 rounded mx-3 text-gray-700 flex-grow shadow-md"
        placeholder="Type here to search..."
      />
      <input
        type="submit"
        value="Search"
        className="px-2 mr-3 cursor-pointer rounded shadow-xs bg-transparent flex-grow border border-blue-800 text-blue-800 hover:bg-blue-800 hover:text-gray-100 md:flex-grow-0"
      />
    </form>
  );
}
